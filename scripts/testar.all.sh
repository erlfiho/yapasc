#!/bin/bash


if test ! -d "$1" || test -z "$1" ;then
    echo "Diretorio '$1' nao existe!"
    exit 1;
fi

for source in $1/*.pas
do
    output_file=$(basename $source | sed s/.pas/.yapc/g)
    echo yapasc $source
    ./yapasc < $source > ./output/$output_file
done
