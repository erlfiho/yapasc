#ifndef __CODE__
#define __CODE__

/* Biliotecas necessarias */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "pilha.h"
#define OUTPUTFILE "output.mepa"
#define IDENTIFICADOR 50
#define ROTULO 4

/*------------------------------ESTRUTURAS--------------------------------*/

/* Tipos de identificador */
typedef enum categoria_e {
        procedimento=0, funcao=1, variavel=2,
        param_valor=3, param_ref=4
        } categoria_e;
typedef enum variavel_e {inteiro=0, booleano=1} variavel_e;

/* elemento da tabela de simbolo */
typedef struct {
    char identificador[IDENTIFICADOR]; /* */
    // char rotulo[ROTULO];  /* */
    unsigned int rotulo;
    categoria_e categoria;
    variavel_e var;
    int nivel_lexico;
    int deslocamento;
    int *parametros;
    int n_parametros;
} simbolo_t ;

/* elemento da memoria */
typedef struct {
    int valor;
} memoria_t;

/* Estruturas usadas */
pilha *memoria;
pilha *tabela_simbolos, *tabela_rotulos;
pilha *rotulos_loops_if;
lista *simbolos;
lista *parametros;
int n_lexico;
int desloc;
simbolo_t *simb, *proc, *func, *last_ident, *tmp;
int rot, exe_rot, rot_cond;
int n_vars, params;
no *resultado;

/*------------------------------FUNCOES----------------------------------*/

/* debug */
void yyerror(char *s);
void debug_int (int i);
void debug_string (char *s);
void debug_char (char c);
void goto_label(int l);
void setup(void);

/* escrita de instrucoes */
void salva (char *instr);

/* tabela de simbolos */
int existe_simbolo (char *s);
int salva_simbolo ( char *ident, unsigned int rotulo, int categ,
                    int nivel, int desl, int param);
simbolo_t * cria_simbolo(char *s, int lex, int desl);
simbolo_t * cria_tipo_int();
simbolo_t * cria_tipo_bool();
void imprimir_simbolos (void);
simbolo_t * retorna_simbolo(pilha * p, char *id);
simbolo_t * copia(simbolo_t *origem);

/* memoria */
int salva_memoria (int v);
void imprimir_memoria (void);
int existe_memoria (int v);

void inicializa_compilador();
int compara_tipo(simbolo_t *a, simbolo_t *b);

#endif
