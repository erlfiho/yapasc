#include <stdio.h>
#ifndef _pilha_
#define _pilha_

#include "lista.h"

typedef lista pilha;

int vazia_pilha(pilha *f);
unsigned int tamanho_pilha(pilha *f);
pilha *constroi_pilha(void);
void destroi_pilha(pilha *f);
int empilha(objeto o, pilha *f);
objeto desempilha(pilha *f);

#endif

