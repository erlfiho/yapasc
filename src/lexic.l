/* Reserved words */
not         NOT|Not|not
goto        GOTO|Goto|goto
begin       BEGIN|Begin|begin
end         END|End|end
if          IF|If|if
then        THEN|Then|then
else        ELSE|Else|else
while       WHILE|While|while
do          DO|Do|do
program     PROGRAM|Program|program
label       LABEL|Label|label
type        TYPE|Type|type
var         VAR|Var|var
procedure   PROCEDURE|Procedure|procedure
function    FUNCTION|Function|function
array       ARRAY|Array|array
of          OF|Of|of
write       WRITE|Write|write
writeln     WRITELN|Writeln|writeln
read        READ|Read|read
readln      READLN|Readln|readln
false       FALSE|False|false
true        TRUE|True|true
/* Signals */
sub         "-"
add         "+"
div         DIV|Div|div
mul         "*"
and         AND|And|and
or          OR|Or|or
less        "<"
equal       "="
greater     ">"
n_equal     "<>"
less_eq     "<="
greater_eq  ">="
atrib       ":="
end_prog    "."
semicolon   ";"
colon       ":"
comma       ","
/* Another expressions */
single_quote    \'
quote           \"
obrace          \{
cbrace          \}
obrackets       \[
cbrackets       \]
oparenth        \(
cparenth        \)
digit           [0-9]
letter          [A-Za-z]
/* tipos */
integer    INTEGER|Integer|integer
boolean    BOOLEAN|Bolean|boolean
%{
    #include <stdio.h>
    #include "sintatic.h"
%}

%%

{not}                               return NOT;
{goto}                              return GOTO;
{begin}                             return BEG;
{end}                               return END;
{if}                                return IF;
{then}                              return THEN;
{else}                              return ELSE;
{while}                             return WHILE;
{do}                                return DO;
{program}                           return PROGRAM;
{label}                             return LABEL;
{type}                              return TYPE;
{var}                               return VAR;
{procedure}                         return PROCEDURE;
{function}                          return FUNCTION;
{array}                             return ARRAY;
{of}                                return OF;
{write}                             return WRITE;
{writeln}                           return WRITELN;
{read}                              return READ;
{readln}                            return READLN;
{false}                             return FALSE;
{true}                              return TRUE;

{sub}                               return *yytext;
{add}                               return *yytext;
{div}                               return *yytext;
{mul}                               return *yytext;
{and}                               return AND;
{or}                                return OR;
{less}                              return LESS;
{equal}                             return EQUAL;
{greater}                           return GREATER;
{n_equal}                           return N_EQUAL;
{less_eq}                           return LESS_EQ;
{greater_eq}                        return GREATER_EQ;
{atrib}                             return *yytext;
{end_prog}                          return *yytext;
{semicolon}                         return *yytext;
{colon}                             return *yytext;
{comma}                             return *yytext;

{single_quote}                      return *yytext;
{quote}                             return *yytext;
{obrace}                            return *yytext;
{cbrace}                            return *yytext;
{obrackets}                         return *yytext;
{cbrackets}                         return *yytext;
{oparenth}                          return *yytext;
{cparenth}                          return *yytext;
{integer}                           {
                                      yylval.string = "int";
                                      return IDENTF;
                                    }

{boolean}                           {
                                      yylval.string = "bool";
                                      return IDENTF;
                                    }

{digit}+                            {
                                      yylval.number = atoi(yytext);
                                      return NUMBER;
                                    }

{letter}({letter}|{digit})*         {
                                      yylval.string = strdup(yytext);
                                      return IDENTF;
                                    }

[ \t]+                              ; /*ignore*/
\n                                  ; /*ignore*/

%%

int yywrap(){
    return 1;
}
