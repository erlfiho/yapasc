#include "code.h"

/**
 * Operacoes para a geracao de codigo
 */

/* funcao de debug */
void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
}

/* funcao de debug */
void debug_int (int i) {
    printf("\ndebug_int: %d\n", i);
}

/* funcao de debug */
void debug_string (char *s) {
    printf("\ndebug_string: %s\n", s);
}

/* funcao de debug */
void debug_char (char c) {
    printf("\ndebug_string: %c\n", c);
}

/* funcao de debug */
void goto_label(int l) {
    printf("\nlabel: %d\n",l);
}

/**
 * Inicia as variaveis necessarias para a compilacao
 */
void setup(void) {
    puts("Configurando variaveis do compilador.");
}

/**
 * Salva instrucao em OUTPUTFILE
 */
void salva (char *instr) {
    FILE *fp;
    fp = fopen(OUTPUTFILE, "a");
    if (!fp)
        printf("erro: Impossivel abrir %s\n",OUTPUTFILE);
    fprintf(fp, "%s", instr);
    fprintf(fp, "\n");
    fclose(fp);
}

/**
 * Imprimi tabela de simbolos
 */
void imprimir_simbolos (void) {
   int i=0, size=0;
   size = tabela_simbolos->tamanho;
   simbolo_t *e;
   no *primeiro;

   puts("-begin-------");
   printf("Tamanho %d\n", size);
   primeiro = tabela_simbolos->primeiro;
   for (i=0; i<size; i++) {
        e = (simbolo_t*)primeiro->o;
        printf("%d: %s - %d - %d \n",e->rotulo, e->identificador,
e->nivel_lexico, e->deslocamento);
        primeiro = primeiro->sucessor;
   }
   puts("-end---------");
}

simbolo_t * retorna_simbolo(pilha * p, char *id)
{
   int i=0, size=0;
   size = p->tamanho;
   simbolo_t *e;
   no *primeiro;

   primeiro = p->primeiro;
   for (i=0; i<size; i++) {
        e = (simbolo_t*)primeiro->o;
        if (strcmp(e->identificador, id) == 0)
            return e;
        if(!primeiro->sucessor)
            return NULL;
        primeiro = primeiro->sucessor;
   }
   return NULL;
}

simbolo_t * copia(simbolo_t *origem)
{
    simbolo_t *tmp = (simbolo_t*) malloc(sizeof(simbolo_t));

    strcpy(tmp->identificador, origem->identificador);
    tmp->rotulo = origem->rotulo;
    tmp->categoria = origem->categoria;
    tmp->var = origem->var;
    tmp->nivel_lexico = origem->nivel_lexico;
    tmp->deslocamento = origem->deslocamento;
    tmp->n_parametros = origem->n_parametros;
    tmp->parametros = (int *) malloc(sizeof(origem->n_parametros));
    memcpy(tmp->parametros, origem->parametros, origem->n_parametros);

    return tmp;
}

/**
 * Busca simbolo na tabela de simbolos
 */
int existe_simbolo (char *s) {
   int i=0, size=0;
   size = tabela_simbolos->tamanho;
   simbolo_t *e;
   no *primeiro;

   primeiro = tabela_simbolos->primeiro;
   for (i=0; i<size; i++) {
        e = (simbolo_t*)primeiro->o;
        if (strcmp(e->identificador, s) == 0)
            return 1;
        if (!primeiro->sucessor)
            return 0;
        primeiro = primeiro->sucessor;
   }
   return 0;
}

simbolo_t * cria_simbolo(char *s, int lex, int desl)
{
    simbolo_t *tmp = (simbolo_t*) malloc(sizeof(simbolo_t));
    strcpy(tmp->identificador, s);
    tmp->rotulo = 0;
    tmp->nivel_lexico = lex;
    tmp->deslocamento = desl;
    tmp->n_parametros = 0;
    tmp->parametros = NULL;

    return tmp;
}

simbolo_t * cria_tipo_int()
{
    simbolo_t *tmp = (simbolo_t*) malloc(sizeof(simbolo_t));
    tmp->var = inteiro;

    return tmp;
}

simbolo_t * cria_tipo_bool()
{
    simbolo_t *tmp = (simbolo_t*) malloc(sizeof(simbolo_t));
    tmp->var = booleano;

    return tmp;
}

/**
 * Salva simbolo na pilha p
 */
int salva_simbolo ( char *ident, unsigned int rotulo, int categ,
                    int nivel, int desl, int param)  {

    simbolo_t *t;
    t = (simbolo_t*)malloc(sizeof(simbolo_t));
    if (!t) return -1;
    strcpy( t->identificador, ident);
    t->rotulo = rotulo;
    t->categoria = categ;
    t->nivel_lexico = nivel;
    t->deslocamento = desl;
    t->n_parametros = param;

    return empilha((objeto)t, tabela_simbolos);
}

/**
 * Imprimi tabela de memoria
 */
void imprimir_memoria (void) {
   int i=0, size=0;
   size = memoria->tamanho;
   memoria_t *e;
   no *primeiro;

   puts("-begin-memoria------");
   primeiro = memoria->primeiro;
   for (i=0; i<size; i++) {
        e = (memoria_t*)primeiro->o;
        printf("%d: %d\n",i , e->valor);
        primeiro = primeiro->sucessor;
   }
   puts("-end-memoria--------");
}



/**
 * Busca na memoria
 */
int existe_memoria (int v) {
   int i=0, size=0;
   size = memoria->tamanho;
   memoria_t *e;
   no *primeiro;

   primeiro = memoria->primeiro;
   for (i=0; i<size; i++) {
        e = (memoria_t*)primeiro->o;
        if (e->valor == v)
            return 1;
        if (!primeiro->sucessor)
            return 0;
        primeiro = primeiro->sucessor;
   }
   return 0;
}

/**
 * Salva na memoria
 */
int salva_memoria (int v)  {
    memoria_t *t;
    t = (memoria_t*)malloc(sizeof(memoria_t));
    if (!t) return -1;
    t->valor = v;
    return empilha((objeto)t, memoria);
}

void inicializa_compilador()
{
    memoria = constroi_pilha();
    tabela_simbolos = constroi_pilha();
    rotulos_loops_if = constroi_pilha();
    tabela_rotulos = constroi_pilha();
    simbolos = constroi_lista();
    parametros = constroi_lista();
    n_lexico = desloc = rot = exe_rot = rot_cond = 0;
    n_vars = params = 0;
    simb = (simbolo_t*) malloc(sizeof(simbolo_t));
    proc = (simbolo_t*) malloc(sizeof(simbolo_t));
    func = (simbolo_t*) malloc(sizeof(simbolo_t));
    last_ident = (simbolo_t*) malloc(sizeof(simbolo_t));
    tmp = (simbolo_t*) malloc(sizeof(simbolo_t));
}

int compara_tipo(simbolo_t *a, simbolo_t *b)
{
    if (a->var == b->var)
        return 1;
    return 0;
}
