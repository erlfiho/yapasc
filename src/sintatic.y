%{
    #include <stdio.h>
    #include "code.h"

    FILE *yyin;
    int yylex(void);
    void yyerror(char *);

    int nivel_lex = 0;
    int deslocamento = 0;
    int n_parametros = 0;
    int *rotulo_tmp = NULL;
    int *rotulo_tmp2 = NULL;
    unsigned int rotulo = 0;
    unsigned int label = 0;
    categoria_e categoria;
    simbolo_t *a, *b;

    /* informacoes gerenciais empilhadas
     * por funcoes e procedimentos */
    int quantidade_vars_bloco = 0 ;

    FILE * saida;

%}

%token NOT GOTO BEG END IF THEN ELSE WHILE DO PROGRAM LABEL TYPE VAR
%token INTEGER BOOLEAN PROCEDURE FUNCTION ARRAY OF WRITE WRITELN READ READLN
%token EQUAL LESS GREATER N_EQUAL LESS_EQ GREATER_EQ OR AND TRUE FALSE

%union
{
    int number;
    char *string;
}

%token <number> NUMBER
%token <string> IDENTF

%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE

%%

programa: PROGRAM {
        fprintf(saida,"\tINPP\n");
    }
    identificador
    argumentos
    ';'
    bloco
    '.' {
        fprintf(saida,"\tPARA\n");
    }
    ;

argumentos: vazio
    | '(' lista_identificadores ')'
    ;

bloco:
       declara_rotulos
       declara_tipos
       declara_variaveis
       declara_subrotinas
       comando_composto {
       n_vars = 0;
       int i = 0;
       printf("vars(%d)\n", quantidade_vars_bloco);
       for( i=0; i < quantidade_vars_bloco; i++) {
           simb = (simbolo_t*) desempilha(tabela_simbolos);
           if (simb->categoria == variavel)
               n_vars++;
       }
       quantidade_vars_bloco = 0;

       if ((simb->categoria == procedimento ||
               simb->categoria == funcao) &&
               simb->nivel_lexico == n_lexico)
           empilha((void*)simb, tabela_simbolos);

       if(n_vars)
           fprintf(saida,"\tDMEM %d\n", n_vars);
       n_vars = 0;
    }
    ;

/*Declaracao de rotulos*/
declara_rotulos: vazio
    | LABEL lista_numeros ';'
    ;
/*Fim declaracao de rotulos*/

/*Declaracao de tipos*/
declara_tipos: vazio
    | TYPE define_tipos ';'
    ;

define_tipos: define_tipo
    | define_tipos ';' define_tipo
    ;

define_tipo: identificador EQUAL tipo
    ;
/*Fim declaracao de tipos*/

/*Declaracao de variaveis*/
declara_variaveis: vazio
    | VAR {
        /*esvazia a lista de simbolos*/
            while(simb = remove_lista(simbolos)) ;
            deslocamento = 0;
        }
        define_variaveis ';'
    ;

define_variaveis: define_variavel
    | define_variaveis ';' define_variavel
    ;

define_variavel: lista_identificadores ':' tipo {
        /*Insere os identificadores na tabela de simbolos*/
        fprintf(saida,"\tAMEM %d\n", simbolos->tamanho);
        //printf("%d variaveis\n", simbolos->tamanho);
        while( (simb = (simbolo_t*)remove_lista(simbolos)) )
        {
            if(existe_simbolo(simb->identificador))
            {
                fprintf(stderr,"ERRO: identificador \"%s\" ja usado.\n",
simb->identificador);
                exit(EXIT_FAILURE);
            }

            simb->categoria = variavel;
            if(!strcmp(yylval.string,"int"))
                simb->var = inteiro;
            else if(!strcmp(yylval.string, "bool"))
                simb->var = booleano;
            else
            {
                fprintf(stderr,"ERRO: tipo \"%s\" nao existe.\n",
yylval.string);
                exit(EXIT_FAILURE);
            }

            empilha((void*)simb, tabela_simbolos);
        //    imprimir_simbolos();
        }
    }
    ;
/*Fim declaracao de variaveis*/

/*Declaracao de subrotinas*/
declara_subrotinas: vazio
    | declara_procedimento
    | declara_funcao
    ;

declara_procedimento: PROCEDURE identificador declara_parametros ';' bloco ';'
    {
    // Nao sei se esta certo:
    // if (!existe_simbolo($4))
    //     salva_simbolo($2, rotulo++, categoria=0, nivel_lex, deslocamento, n_parametros );
    // imprimir_simbolos();
    }
    ;

declara_funcao: FUNCTION identificador declara_parametros ':' identificador ';'
    {
    // if (!existe_simbolo($2))
    //     salva_simbolo($2, rotulo++, categoria=0, nivel_lex, deslocamento, n_parametros );
    // imprimir_simbolos();
    }
    ;
/*Fim declaracao de rotinas*/

/*Definicao de parametros*/
declara_parametros: vazio
    | '(' define_parametros ')'
    ;

define_parametros: define_parametro
    | define_parametros ';' define_parametro
    ;

define_parametro: lista_identificadores ':' identificador
    | VAR lista_identificadores ':' identificador
    | FUNCTION lista_identificadores ':' identificador
    | PROCEDURE lista_identificadores
    ;
/*Fim definicao de parametros*/

/*Definicao de comando*/
comando_composto: BEG lista_comandos END
    ;

lista_comandos: vazio
    | comando comandos
    ;

comandos: vazio
    | ';' vazio
    | ';' comando comandos
    ;

comando: numero ':' comando_sem_rotulo
    | comando_sem_rotulo
    ;

comando_sem_rotulo:
    comando_composto
    | atribuicao
    | chamada_procedimento
    | desvio
    | comando_condicional
    | comando_repetitivo
    ;
/*Fim definicao de comando*/

/*Definicao comando_sem_rotulo*/
atribuicao: variavel {
        if(last_ident && last_ident->categoria != rotulo)
            empilha((void*)last_ident, memoria);
        puts("empilhou atrib");
    }
    ':=' expressao {
        b = desempilha(memoria);
        a = desempilha(memoria);
        if(compara_tipo(a,b))
            if(a->categoria != param_ref)
                fprintf(saida,"\tARMZ %d,%d  # %s\n", a->nivel_lexico,
                        a->deslocamento, a->identificador);
            else
                fprintf(saida,"\tARMI %d,%d  # %s\n", a->nivel_lexico,
                        a->deslocamento, a->identificador);
        else
        {
            fprintf(stderr,"Tipos nao sao iguais\n");
            exit(EXIT_FAILURE);
        }
    }
    ;

chamada_procedimento: identificador lista_expr_op
    ;

/* nao esta salvando DSVS - talvez nao esteja entrando aqui */
desvio: GOTO numero
    ;

comando_condicional: IF expressao THEN {
        fprintf(saida,"DSVF R%02d\n", rotulo);
        rotulo_tmp = (int*)malloc(sizeof(int)); /* tem que dar free() depois */
        *rotulo_tmp = rotulo;
        empilha((void*)rotulo_tmp, rotulos_loops_if);
        rotulo++;
    } comando_sem_rotulo {
        fprintf(saida,"DSVS R%02d\n", rotulo);
        rotulo_tmp = desempilha(rotulos_loops_if);
        fprintf(saida, "R%02d:\tNADA\n", *rotulo_tmp);

        rotulo_tmp = (int*)malloc(sizeof(int)); /* tem que dar free() depois */
        *rotulo_tmp = rotulo;
        empilha((void*)rotulo_tmp, rotulos_loops_if);
        rotulo++;
    } comando_else {
        rotulo_tmp = desempilha(rotulos_loops_if);
        fprintf(saida, "R%02d:\tNADA\n", *rotulo_tmp);
    }
    ;

comando_else: ELSE comando_sem_rotulo
    | %prec LOWER_THAN_ELSE
    ;

comando_repetitivo: WHILE {
        /* rotulo de inicio do while */
        fprintf(saida,"R%02d:NADA\n", rotulo);
    } expressao {
        rotulo_tmp = (int*)malloc(sizeof(int)); /* tem que dar free() depois */
        *rotulo_tmp = rotulo;
        empilha((void*)rotulo_tmp, rotulos_loops_if);
        rotulo++;

        /* desvia para rotulo fim do while */
        fprintf(saida,"DSVF R%02d\n",rotulo);
        rotulo_tmp = (int*)malloc(sizeof(int)); /* tem que dar free() depois */
        *rotulo_tmp = rotulo;
        empilha((void*)rotulo_tmp, rotulos_loops_if);
        rotulo++;

    } DO comando_sem_rotulo {
        rotulo_tmp = desempilha(rotulos_loops_if);
        rotulo_tmp2 = desempilha(rotulos_loops_if);
        fprintf(saida,"DSVS R%02d\n", *rotulo_tmp2);
        fprintf(saida, "R%02d:\tNADA\n", *rotulo_tmp);
    }
    ;
/*Fim definicao comando_sem_rotulo*/

lista_expr_op: '(' lista_expressoes ')'
    | '(' ')'
    ;

lista_expressoes: expressao ou_mais_expr

ou_mais_expr: vazio
    | ',' expressao ou_mais_expr

/*Definicao de expressao*/
expressao: expressao_simples
    | expressao_simples EQUAL expressao_simples { fprintf(saida, "\tCMIG\n"); }
    | expressao_simples N_EQUAL expressao_simples { fprintf(saida, "\tCMDG\n"); }
    | expressao_simples LESS expressao_simples  { fprintf(saida, "\tCMME\n"); }
    | expressao_simples GREATER expressao_simples  { fprintf(saida, "\tCMMA\n"); }
    | expressao_simples LESS_EQ expressao_simples  { fprintf(saida, "\tCMEG\n"); }
    | expressao_simples GREATER_EQ expressao_simples  { fprintf(saida, "\tCMAG\n"); }
    ;

expressao_simples: termo op_termo
    | '+' termo op_termo
    | '-' termo op_termo { fprintf(saida,"\tINVR\n"); }
    ;

op_termo: vazio
    | '+' termo {
        a = desempilha(memoria);
        b = desempilha(memoria);

        if(compara_tipo(a,b))
            fprintf(saida,"\tSOMA\n");
        else
        {
            fprintf(stderr,"ERRO: tipos diferentes\n");
            exit(EXIT_FAILURE);
        }

        /*Empilha o tipo da soma*/
        tmp = cria_tipo_int();
        empilha((void*)tmp, memoria);
        puts("empilhou soma");
    } op_termo
    | '-' termo {
        a = desempilha(memoria);
        b = desempilha(memoria);

        if(compara_tipo(a,b))
            fprintf(saida,"\tSUBT\n");
        else
        {
            fprintf(stderr,"ERRO: tipos diferentes\n");
            exit(EXIT_FAILURE);
        }

        /*Empilha o tipo da soma*/
        tmp = cria_tipo_int();
        empilha((void*)tmp, memoria);
        puts("empilhou sub");
    } op_termo
    | OR termo {
        a = desempilha(memoria);
        b = desempilha(memoria);

        if(compara_tipo(a,b))
            fprintf(saida,"\tDISJ\n");
        else
        {
            fprintf(stderr,"ERRO: tipos diferentes\n");
            exit(EXIT_FAILURE);
        }

        /*Empilha o tipo da soma*/
        tmp = cria_tipo_bool();
        empilha((void*)tmp, memoria);
    } op_termo
    ;
/*Fim definicao de expressao*/

/*Definicao de termo*/
termo: fator op_fator
    ;

op_fator: vazio
    | '*' fator {
        a = desempilha(memoria);
        b = desempilha(memoria);

        if(compara_tipo(a,b))
            fprintf(saida,"\tMULT\n");
        else
        {
            fprintf(stderr,"ERRO: tipos diferentes\n");
            exit(EXIT_FAILURE);
        }

        /*Empilha o tipo da soma*/
        tmp = cria_tipo_int();
        empilha((void*)tmp, memoria);
    } op_fator
    | '/' fator {
        a = desempilha(memoria);
        b = desempilha(memoria);

        if(compara_tipo(a,b))
            fprintf(saida,"\tDIVI\n");
        else
        {
            fprintf(stderr,"ERRO: tipos diferentes\n");
            exit(EXIT_FAILURE);
        }

        /*Empilha o tipo da soma*/
        tmp = cria_tipo_int();
        empilha((void*)tmp, memoria);
    } op_fator
    | AND fator {
        a = desempilha(memoria);
        b = desempilha(memoria);

        if(compara_tipo(a,b))
            fprintf(saida,"\tCONJ\n");
        else
        {
            fprintf(stderr,"ERRO: tipos diferentes\n");
            exit(EXIT_FAILURE);
        }

        /*Empilha o tipo da soma*/
        tmp = cria_tipo_bool();
        empilha((void*)tmp, memoria);
    } op_fator
    ;

fator: variavel {
        /*Verifica se os tipos das variaveis conferem*/
        if(last_ident)
        {
            if( (last_ident->categoria == variavel) ||
                (last_ident->categoria == param_valor) )
            {
                    fprintf(saida,"\tCRVL %d,%d  # %s\n",
                    last_ident->nivel_lexico,
                    last_ident->deslocamento,
                    last_ident->identificador);
            }

            else if ( (last_ident->categoria == param_ref) )
            {
                    fprintf(saida,"\tCREN %d,%d  # %s\n",
                            last_ident->nivel_lexico,
                            last_ident->deslocamento,
                            last_ident->identificador);
            }
        }
        else if(!last_ident)
        {
            fprintf(stderr,"ERRO: Identificador \"%s\" nao existe.\n",
                    yylval.string);
            exit(EXIT_FAILURE);
        }
        empilha((void*)last_ident, memoria);
        puts("empilhou var");
    }
    | NUMBER {
        fprintf(saida, "\tCRCT %d\n", yylval.number);
        tmp = cria_tipo_int();
        empilha((void*)tmp, memoria);
        puts("empilhou numero");
    }
    | TRUE {
        fprintf(saida, "\tCRCT 1 # TRUE\n");
        tmp = cria_tipo_bool();
        empilha((void*)tmp, memoria);
        puts("empilhou bool");
    }
    | FALSE {
        fprintf(saida, "\tCRCT 0 # FALSE\n");
        tmp = cria_tipo_bool();
        empilha((void*)tmp, memoria);
        puts("empilhou bool");
    }
    | chamada_funcao
    | '(' expressao ')'
    | NOT fator {
        fprintf(saida, "\tNEGA\n");
    }
    ;
/*Fim definicao de termo*/

chamada_funcao: chamada_procedimento
    ;

tipo: identificador
    ;

variavel: identificador
    ;

lista_identificadores: identificador {
        /*cria simbolo e adiciona na lista de identificadores*/
            simb = cria_simbolo(yylval.string, n_lexico, deslocamento);
            resultado = insere_lista((void*)simb, simbolos);
            deslocamento++;
            quantidade_vars_bloco++;
        }
    continua_lista_identificadores
    ;

continua_lista_identificadores: vazio
    | ',' identificador {
        /*cria simbolo e adiciona na lista de identificadores*/
            simb = cria_simbolo(yylval.string, n_lexico, deslocamento);
            resultado = insere_lista((void*)simb, simbolos);
            deslocamento++;
            quantidade_vars_bloco++;
    }
    ;

identificador: IDENTF
    {
        last_ident = retorna_simbolo(tabela_simbolos, yylval.string);
        if(last_ident)
            printf("Simbolo atual = %s\n", last_ident->identificador);
        /*if (existe_simbolo($1))
        {
            fprintf(stderr,"ERRO: Identificador %s ja foi declarado\n", $1);
            exit(EXIT_FAILURE);
        }
        else
            salva_simbolo( $1, rotulo++, categoria, nivel_lex, deslocamento, n_parametros );
        imprimir_simbolos();*/
    }
    ;

lista_numeros: numero
    | lista_numeros ',' numero
    ;

numero: NUMBER { /*debug_int($1);*/ }
    ;

vazio:
    ;

%%

int main(int argc, char *argv[]) {
    if ( argc != 2 )
    {
        fprintf(stderr,"Uso: ./yapasc <arquivo.pas>\n");
        exit(EXIT_FAILURE);
    }
    else if ( !(yyin = fopen(argv[1],"r"))  )
    {
        fprintf(stderr,"Não foi possível abrir arquivo %s.\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    saida = fopen(OUTPUTFILE, "w");

    inicializa_compilador();
    yyparse();

    fclose(saida);
    return 0;
}
