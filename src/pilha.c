#include "pilha.h"

int vazia_pilha(pilha *f)
{
        return vazia_lista(f);
}

unsigned int tamanho_pilha(pilha *f)
{
        return tamanho_lista(f);
}

pilha *constroi_pilha(void)
{
        return constroi_lista();
}

void destroi_pilha(pilha *f)
{
        destroi_lista(f);
}

int empilha(objeto o, pilha *f)
{
        return insere_lista(o,f) != NULL;
}

objeto desempilha(pilha *f)
{
        return remove_lista(f);
}
