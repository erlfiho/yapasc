#!/bin/bash

if ! test -L yapasc ;then
    echo "Compiling MEPA Compiler."
    make
    echo "Done."
fi

echo "Compiling test programs..."
for source in test/*.pas; do
    dest=$(echo $source | sed s/pas/mepa/g)
    ./yapasc $source
    mv output.mepa test/$dest
    echo "--> test/$dest"
done
echo "Done."
