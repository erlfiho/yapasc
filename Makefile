SRC=src
BIN=bin

#------------------------------------------------------------------------------
.PHONY : all clean

#------------------------------------------------------------------------------
all: yapasc

yapasc: bison flex lista.o pilha.o code.o
	@if test ! -d $(BIN); then mkdir $(BIN); fi
	@gcc $(SRC)/lexic.c $(SRC)/sintatic.c $(SRC)/lista.o $(SRC)/pilha.o $(SRC)/code.o -o $(BIN)/yapasc
	@chmod +x $(BIN)/yapasc
	@if test ! -L yapasc; then ln -s $(BIN)/yapasc yapasc ;fi

bison:
	@bison -y -d -o $(SRC)/sintatic.c $(SRC)/sintatic.y

flex:
	@flex --outfile $(SRC)/lexic.c $(SRC)/lexic.l

lista.o:
	@$(CC) -c $(SRC)/lista.c -o $(SRC)/lista.o

pilha.o:
	@$(CC) -c $(SRC)/pilha.c -o $(SRC)/pilha.o

code.o:
	@$(CC) -c $(SRC)/code.c -o $(SRC)/code.o

clean:
	@rm -rf bin
	@rm -f yapasc $(SRC)/lexic.h $(SRC)/lexic.c
	@rm -f yapasc $(SRC)/sintatic.c $(SRC)/sintatic.h
	@rm -f $(SRC)/*.o
	@rm -f output.mepa
